<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    use HasFactory;
    protected $table = 'dosen';
    protected $primaryKey = 'id';
    protected $fillable = ['nip', 'nama', 'alamat'];

    public function scopeFilter($query)
    {
        if(request('search')){
            return $query->where('nama', 'like', '%'. request('search').'%')
                         ->orWhere('nip', 'like', '%'. request('search').'%')
                         ->orWhere('alamat', 'like', '%'. request('search').'%');
        }
    }
}
