<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Makanan extends Model
{
    use HasFactory;
    protected $table = 'makanan';
    protected $primaryKey = 'id';
    protected $fillable = ['kode', 'nama', 'harga'];

    public function scopeFilter($query)
    {
        if(request('search')){
            return $query->where('nama', 'like', '%'. request('search').'%')
                         ->orWhere('kode', 'like', '%'. request('search').'%')
                         ->orWhere('harga', 'like', '%'. request('search').'%');
        }
    }
}
