<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Petshop extends Model
{
    use HasFactory;
    protected $table = 'petshop';
    protected $primaryKey = 'id';
    protected $fillable = ['kode', 'nama', 'jenis'];

    public function scopeFilter($query)
    {
        if(request('search')){
            return $query->where('nama', 'like', '%'. request('search').'%')
                         ->orWhere('kode', 'like', '%'. request('search').'%')
                         ->orWhere('jenis', 'like', '%'. request('search').'%');
        }
    }
}
