<?php

namespace App\Http\Controllers;

use App\Models\Petshop;
use Illuminate\Http\Request;

class PetshopController extends Controller
{
    public function index()
    {
        return view ('petshop.index',[
        "petshop"=> Petshop::oldest()->filter()->paginate(5)->withQueryString()
        ]);
    }
    public function create()
    {
        return view('petshop.create');
    }
    public function store(Request $request)
    {
        $input = $request->all();
        Petshop::create($input);
        return redirect('petshop')->with('flash_message', 'Data Berhasi Ditambahkan!');
    }

    public function show($id)
    {
        $petshop = Petshop::find($id);
        return view('petshop.show')->with('petshop', $petshop);
    }

    public function edit($id)
    {
        $petshop = Petshop::find($id);
        return view('petshop.edit')->with('petshop', $petshop);
    }

    public function update(Request $request, $id)
    {
        $petshop = Petshop::find($id);
        $input = $request->all();
        $petshop->update($input);
        return redirect('petshop')->with('flash_message', 'Data Berhasil Diperbaharui!');
    }

    public function destroy($id)
    {
        Petshop::destroy($id);
        return redirect('petshop')->with('flash_message', 'Data Berhasi Dihapus!');
    }
}
