<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;

class ApiPostsController extends Controller
{
    public function index(request $request)
    {
        $data = Post::latest()->filter()->paginate(5)->withQueryString();

        return response()->json($data, 200);
    }
}
