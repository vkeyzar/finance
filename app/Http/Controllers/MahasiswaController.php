<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:mahasiswa-list|mahasiswa-create|mahasiswa-edit|mahasiswa-delete', ['only' => ['index','show']]);
         $this->middleware('permission:mahasiswa-create', ['only' => ['create','store']]);
         $this->middleware('permission:mahasiswa-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:mahasiswa-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        return view ('mahasiswa.index',[
        "mahasiswa"=> Mahasiswa::oldest()->filter()->paginate(5)->withQueryString()
        ]);
    }
    public function create()
    {
        return view('mahasiswa.create');
    }
    public function store(Request $request)
    {
        $input = $request->all();
        Mahasiswa::create($input);
        return redirect('mahasiswa')->with('flash_message', 'Data Berhasi Ditambahkan!');
    }

    public function show($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        return view('mahasiswa.show')->with('mahasiswa', $mahasiswa);
    }

    public function edit($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        return view('mahasiswa.edit')->with('mahasiswa', $mahasiswa);
    }

    public function update(Request $request, $id)
    {
        $mahasiswa = Mahasiswa::find($id);
        $input = $request->all();
        $mahasiswa->update($input);
        return redirect('mahasiswa')->with('flash_message', 'Data Berhasil Diperbaharui!');
    }

    public function destroy($id)
    {
        Mahasiswa::destroy($id);
        return redirect('mahasiswa')->with('flash_message', 'Data Berhasi Dihapus!');
    }
}
