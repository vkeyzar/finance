<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function index()
    {
        return view ('dosen.index',[
        "dosen"=> Dosen::oldest()->filter()->paginate(5)->withQueryString()
        ]);
    }
    public function create()
    {
        if (auth()->user()->admin  || auth()->user()->username == "Muhammadsyauqi"){
            return view('dosen.create');
        }
        else{
            return abort(403);
        }
        
    }
    public function store(Request $request)
    {
        $input = $request->all();
        Dosen::create($input);
        return redirect('dosen')->with('flash_message', 'Data Berhasi Ditambahkan!');
    }

    public function show($id)
    {
        $dosen = Dosen::find($id);
        return view('dosen.show')->with('dosen', $dosen);
    }

    public function edit($id)
    {
        $dosen = Dosen::find($id);
        if (auth()->user()->admin  || auth()->user()->username == "Muhammadsyauqi"){
            return view('dosen.edit')->with('dosen', $dosen);
        }
        else{
            return abort(403);
        }
       
    }

    public function update(Request $request, $id)
    {
        $dosen = Dosen::find($id);
        $input = $request->all();
        $dosen->update($input);
        return redirect('dosen')->with('flash_message', 'Data Berhasil Diperbaharui!');
    }

    public function destroy($id)
    {
        Dosen::destroy($id);
        return redirect('dosen')->with('flash_message', 'Data Berhasi Dihapus!');
    }
}
