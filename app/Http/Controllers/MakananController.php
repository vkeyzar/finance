<?php

namespace App\Http\Controllers;

use App\Models\Makanan;
use Illuminate\Http\Request;

class MakananController extends Controller
{
    public function index()
    {
        return view ('makanan.index',[
        "makanan"=> Makanan::oldest()->filter()->paginate(5)->withQueryString()
        ]);
    }
    public function create()
    {
        return view('makanan.create');
    }
    public function store(Request $request)
    {
        $input = $request->all();
        Makanan::create($input);
        return redirect('makanan')->with('flash_message', 'Data Berhasi Ditambahkan!');
    }

    public function show($id)
    {
        $makanan = Makanan::find($id);
        return view('makanan.show')->with('makanan', $makanan);
    }

    public function edit($id)
    {
        $makanan = Makanan::find($id);
        return view('makanan.edit')->with('makanan', $makanan);
    }

    public function update(Request $request, $id)
    {
        $makanan = Makanan::find($id);
        $input = $request->all();
        $makanan->update($input);
        return redirect('makanan')->with('flash_message', 'Data Berhasil Diperbaharui!');
    }

    public function destroy($id)
    {
        Makanan::destroy($id);
        return redirect('makanan')->with('flash_message', 'Data Berhasi Dihapus!');
    }
}
