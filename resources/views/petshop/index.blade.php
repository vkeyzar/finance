@extends('petshop.dashboard')
@section('content')
    <div class="container">
           
        <div class="row">
 
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h2>Data Petshopku</h2>
                    </div>
                    <br/>
                    <div class="row justify-content-center">
                    <div class="col-md-6">
                        <form action="/petshop">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Search" name="search" value="{{request('search')}}">
                                <button class="btn btn-secondary" type="submit">Search</button>
                            </div>
                        </form>
                    </div>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/petshop/create') }}" class="btn btn-success btn-sm" title="Add">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                       <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Hewan</th>
                                        <th>Kode Petshop</th>
                                        <th>Jenis Hewan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($petshop as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->kode }}</td>
                                        <td>{{ $item->jenis }}</td>
 
                                        <td>
                                            <a href="{{ url('/petshop/' . $item->id) }}" title="View"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/petshop/' . $item->id . '/edit') }}" title="Edit"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
 
                                            <form method="POST" action="{{ url('/petshop' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
 
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ $petshop->links() }}
@endsection