@extends('petshop.dashboard')
@section('content')
 
<div class="card">
  <div class="card-header">Petshop</div>
  <div class="card-body">
      
      <form action="{{ url('/petshop') }}" method="post">
        {!! csrf_field() !!}
        <label>Nama Hewan</label></br>
        <input type="text" name="nama" id="nama" class="form-control"></br>
        <label>Kode Petshop</label></br>
        <input type="text" name="kode" id="kode" class="form-control"></br>
        <label>Jenis Hewan</label></br>
        <input type="text" name="jenis" id="jenis" class="form-control"></br>
        <input type="submit" value="Save" class="btn btn-success"></br>
    </form>
   
  </div>
</div>
 
@stop