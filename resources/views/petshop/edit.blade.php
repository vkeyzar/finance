@extends('petshop.dahsboard')
@section('content')
 
<div class="card">
  <div class="card-header">Edit Data</div>
  <div class="card-body">
      
      <form action="{{ url('petshop/' .$petshop->id) }}" method="post">
        {!! csrf_field() !!}
        @method("PATCH")
        <input type="hidden" name="id" id="id" value="{{$petshop->id}}" id="id" />
        <label>Nama</label></br>
        <input type="text" name="nama" id="nama" value="{{$petshop->nama}}" class="form-control"></br>
        <label>NIM</label></br>
        <input type="text" name="kode" id="kode" value="{{$petshop->kode}}" class="form-control"></br>
        <label>Alamat</label></br>
        <input type="text" name="jenis" id="jenis" value="{{$petshop->jenis}}" class="form-control"></br>
        <input type="submit" value="Update" class="btn btn-success"></br>
    </form>
   
  </div>
</div>
 
@stop