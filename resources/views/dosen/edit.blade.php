@extends('dosen.dashboard')
@section('content')
 
<div class="card">
  <div class="card-header">Edit Data</div>
  <div class="card-body">
      
      <form action="{{ url('dosen/' .$dosen->id) }}" method="post">
        {!! csrf_field() !!}
        @method("PATCH")
        <input type="hidden" name="id" id="id" value="{{$dosen->id}}" id="id" />
        <label>Nama</label></br>
        <input type="text" name="nama" id="nama" value="{{$dosen->nama}}" class="form-control"></br>
        <label>NIP</label></br>
        <input type="text" name="nip" id="nip" value="{{$dosen->nip}}" class="form-control"></br>
        <label>Alamat</label></br>
        <input type="text" name="alamat" id="alamat" value="{{$dosen->alamat}}" class="form-control"></br>
        <input type="submit" value="Update" class="btn btn-success"></br>
    </form>
   
  </div>
</div>
 
@stop