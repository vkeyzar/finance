@extends('dosen.dashboard')
@section('content')
 
<div class="card">
  <div class="card-header">Dosen</div>
  <div class="card-body">
      
      <form action="{{ url('/dosen') }}" method="post">
        {!! csrf_field() !!}
        <label>Nama</label></br>
        <input type="text" name="nama" id="nama" class="form-control"></br>
        <label>NIP</label></br>
        <input type="text" name="nip" id="nip" class="form-control"></br>
        <label>Alamat</label></br>
        <input type="text" name="alamat" id="alamat" class="form-control"></br>
        <input type="submit" value="Save" class="btn btn-success"></br>
    </form>
   
  </div>
</div>
 
@stop