@extends('makanan.dashboard')
@section('content')
 
<div class="card">
  <div class="card-header">Makanan</div>
  <div class="card-body">
      
      <form action="{{ url('/makanan') }}" method="post">
        {!! csrf_field() !!}
        <label>Nama</label></br>
        <input type="text" name="nama" id="nama" class="form-control"></br>
        <label>Kode</label></br>
        <input type="text" name="kode" id="kode" class="form-control"></br>
        <label>Harga</label></br>
        <input type="text" name="harga" id="harga" class="form-control"></br>
        <input type="submit" value="Save" class="btn btn-success"></br>
    </form>
   
  </div>
</div>
 
@stop