@extends('makanan.dashboard')
@section('content')
 
<div class="card">
  <div class="card-header">Edit Data</div>
  <div class="card-body">
      
      <form action="{{ url('makanan/' .$makanan->id) }}" method="post">
        {!! csrf_field() !!}
        @method("PATCH")
        <input type="hidden" name="id" id="id" value="{{$makanan->id}}" id="id" />
        <label>Nama</label></br>
        <input type="text" name="nama" id="nama" value="{{$makanan->nama}}" class="form-control"></br>
        <label>Kode</label></br>
        <input type="text" name="kode" id="kode" value="{{$makanan->kode}}" class="form-control"></br>
        <label>Harga</label></br>
        <input type="text" name="harga" id="harga" value="{{$makanan->harga}}" class="form-control"></br>
        <input type="submit" value="Update" class="btn btn-success"></br>
    </form>
   
  </div>
</div>
 
@stop