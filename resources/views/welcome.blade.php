<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Kelompok 8</title>
    <style>
      @font-face {
        font-family: 'Poppins';
        src: 'fonts/Poppins-Bold.ttf';
      }

      :root {
        --main-bg-color: #fb9300;
        --main-text-color: #1e375b;
        --second-text-color: #000000;
        --second-bg-color: #f5e6ca;
        --main-font: 'Poppins';
      }

      body {
        background-color: var(--second-bg-color);
      }

      a {
        text-decoration: none;
        color: var(--main-bg-color);
      }

      .page-content-wrapper {
        min-width: 100vw;
      }

      .main-title,
      .main-content {
        font-family: var(--main-font);
      }

      .navbar {
        font-family: var(--main-font);
        background-color: var(--main-bg-color);
        font-size: 1.2em;
      }
      .judul-desc h1 {
        font-family: var(--main-font);
        margin-top: 30px;
        font-size: 72px;
      }
      .kartu {
        width: 360px;
        height: 320px;
        border: 8px solid;
        border-radius: 35px;
        padding: 1rem;
        font-size: 24px;
        margin-bottom: 20px;
      }

      .kartu-panjang {
        font-size: 20px;
      }

      .icon {
        margin-top: 1rem;
      }

      .row {
        margin-top: 4rem;
      }
      .zoom:hover {
        transition: transform 0.2s;
        -ms-transform: scale(1.1); /* IE 9 */
        transform: scale(1.1);
      }
      .nav-link:hover::after {
        content: '';
        display: block;
        border-bottom: 3px solid #ffffff;
        border-radius: 2px 2px;
        width: 55%;
        margin: auto;
        margin-bottom: -10px;
      }

    </style>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-dark shadow sticky-top fw-bold">
    <div class="container">
      <div class="container-fluid">
        <a class="navbar-brand" href="/">
          Kelompok 8
        </a>
      </div>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ms-auto">
          <li class="nav-item mx-3">
            <a class="nav-link active" aria-current="page" href="/">Home</a>
          </li>
          <li class="nav-item mx-3">
            <a class="nav-link active" aria-current="page" href="/">About</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="isi-konten">
    <div class="judul-desc text-center">
      <h1 class="fw-bold">Kelompok 8</h1>
    </div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-4 text-center zoom">
          <div class="kartu">
            <a href="/dashboardposts">
              <div class="card-body">
                <p class="card-text fw-bold">Blog</p>
              </div>
            </a>
          </div>
        </div>
        <div class="col-4 text-center zoom">
          <div class="kartu">
            <a href="/dashboarddosen">
              <div class="card-body">
                <p class="card-text fw-bold ">Dosen</p>
              </div>
            </a>
          </div>
        </div>
        <div class="col-4 text-center zoom">
          <div class="kartu kartu-panjang">
            <a href="/dashboardmahasiswa">
              <div class="card-body">
                <p class="card-text fw-bold ">Mahasiswa</p>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-4 text-center zoom">
          <div class="kartu">
            <a href="/dashboardpetshop">
              <div class="card-body">
                <p class="card-text fw-bold ">Pet Shop</p>
              </div>
            </a>
          </div>
        </div>
        <div class="col-4 text-center zoom">
          <div class="kartu">
            <a href="/dashboardmakanan">
              <div class="card-body">
                <p class="card-text fw-bold ">Makanan</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>