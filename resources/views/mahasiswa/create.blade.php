@extends('mahasiswa.layout')
@section('content')
 
<div class="card">
  <div class="card-header">Mahasiswa</div>
  <div class="card-body">
      
      <form action="{{ url('/mahasiswa') }}" method="post">
        {!! csrf_field() !!}
        <label>Nama</label></br>
        <input type="text" name="nama" id="nama" class="form-control"></br>
        <label>NIM</label></br>
        <input type="text" name="nim" id="nim" class="form-control"></br>
        <label>Alamat</label></br>
        <input type="text" name="alamat" id="alamat" class="form-control"></br>
        <input type="submit" value="Save" class="btn btn-success"></br>
    </form>
   
  </div>
</div>
 
@stop