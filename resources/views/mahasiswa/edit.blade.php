@extends('mahasiswa.layout')
@section('content')
 
<div class="card">
  <div class="card-header">Edit Data</div>
  <div class="card-body">
      
      <form action="{{ url('mahasiswa/' .$mahasiswa->id) }}" method="post">
        {!! csrf_field() !!}
        @method("PATCH")
        <input type="hidden" name="id" id="id" value="{{$mahasiswa->id}}" id="id" />
        <label>Nama</label></br>
        <input type="text" name="nama" id="nama" value="{{$mahasiswa->nama}}" class="form-control"></br>
        <label>NIM</label></br>
        <input type="text" name="nim" id="nim" value="{{$mahasiswa->nim}}" class="form-control"></br>
        <label>Alamat</label></br>
        <input type="text" name="alamat" id="alamat" value="{{$mahasiswa->alamat}}" class="form-control"></br>
        <input type="submit" value="Update" class="btn btn-success"></br>
    </form>
   
  </div>
</div>
 
@stop