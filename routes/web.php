<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostAuthController;
use App\Http\Controllers\MakananAuthController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\MahasiswaAuthController;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\DosenAuthController;
use App\Http\Controllers\MakananController;
use App\Http\Controllers\PetshopController;
use App\Http\Controllers\PetshopAuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;

Route::resource('posts', PostController::class);
Route::get('dashboardposts', [PostAuthController::class, 'dashboard']);
Route::get('loginposts', [PostAuthController::class, 'index'])->name('login');
Route::post('custom-login', [PostAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registrationposts', [PostAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [PostAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [PostAuthController::class, 'signOut'])->name('signout');

Route::resource('mahasiswa', MahasiswaController::class);
Route::get('dashboardmahasiswa', [MahasiswaAuthController::class, 'dashboard']);
Route::get('loginmahasiswa', [MahasiswaAuthController::class, 'index'])->name('login');
Route::post('custom-login', [MahasiswaAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registrationmahasiswa', [MahasiswaAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [MahasiswaAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [MahasiswaAuthController::class, 'signOut'])->name('signout');

Route::resource('dosen', DosenController::class);
Route::get('dashboarddosen', [DosenAuthController::class, 'dashboard']);
Route::get('logindosen', [DosenAuthController::class, 'index'])->name('login');
Route::post('custom-login', [DosenAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registrationdosen', [DosenAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [DosenAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [DosenAuthController::class, 'signOut'])->name('signout');

Route::resource('makanan', MakananController::class);
Route::get('dashboardmakanan', [MakananAuthController::class, 'dashboard']);
Route::get('loginmakanan', [MakananAuthController::class, 'index'])->name('login');
Route::post('custom-login', [MakananAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registrationmakanan', [MakananAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [MakananAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [MakananAuthController::class, 'signOut'])->name('signout');

Route::resource('petshop', PetshopController::class);
Route::get('dashboardpetshop', [PetshopAuthController::class, 'dashboard']);
Route::get('loginpetshop', [PetshopAuthController::class, 'index'])->name('login');
Route::post('custom-login', [PetshopAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registrationpetshop', [PetshopAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [PetshopAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [PetshopAuthController::class, 'signOut'])->name('signout');


Route::get('/', function() {
  return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware' => ['auth']], function() {
  Route::resource('roles', RoleController::class);
  Route::resource('users', UserController::class);
  Route::resource('mahasiswa', MahasiswaController::class);
}
);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::middleware('role:admin')->get('/dashboard', function(){
  return 'Dashboard';
})->name('dashboard');

Route::middleware('role:user')->get('/', function(){
  return 'Welcome';
})->name('welcome');